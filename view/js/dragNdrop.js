jQuery(function($) {
    console.log('dragNdrop is running');

    var dragMethods = {
        mouseStillDown : false,
        itemNow : null,
        startPosition : 0,
        moveAt : function(e) {
            if(dragMethods.itemNow !== null) {
                var top = e.pageY - dragMethods.itemNow.outerHeight() * 2 + 7;
                dragMethods.itemNow.css('top', top + 'px');
                if(dragMethods.startPosition === 0) {
                    dragMethods.startPosition = top;
                } else {
                    var position = top - dragMethods.startPosition;
                    var itemHeight = dragMethods.itemNow.outerHeight();
                    var kef = itemHeight * 1.5;

                    if(position > 0) {
                        // Step +
                        if(position > dragMethods.itemNow.outerHeight() && position < kef) {
                            dragMethods.removeBlankItems();
                            dragMethods.moveItem(1);
                            dragMethods.startPosition = 0;
                        }
                    } else {
                        var positive_position = position * -1;
                        // Step -
                        if(positive_position > dragMethods.itemNow.outerHeight() && positive_position < kef) {
                            dragMethods.removeBlankItems();
                            dragMethods.moveItem(-1);
                            dragMethods.startPosition = 0;
                        }
                    }
                }
            }
        },
        moveItem : function(position) {
            var savePos = null;
            $('.password__item').each(function(pos, item) {
                if($(item).hasClass('password__item_drag')) {
                    savePos = pos;
                }
            });

            if(savePos !== null) {
                savePos += position;
                var itemEq = '.password__item:eq(' + savePos + ')';
                if(position > 0) {
                    $(dragMethods.itemNow).insertAfter(itemEq);
                } else {
                    $(dragMethods.itemNow).insertBefore(itemEq);
                }
                dragMethods.addBlankItem();
            }
        },
        addBlankItem : function() {
            var thLength = $('.password__table th').length;
            var height = dragMethods.itemNow.outerHeight() - 1;
            var blankHtml = '<tr class="password__item password__item_blank">' +
                '<td colspan="' + thLength + '" style="height: ' + height + 'px"></td>' +
                '</tr>';

            $(blankHtml).insertAfter(dragMethods.itemNow);
        },
        removeBlankItems : function() {
            $('.password__item_blank').remove();
        },
        setSizes : function() {
            var item = dragMethods.itemNow;

            if(item !== null) {
                // Set sizes
                item.find('td').each(function() {
                    $(this).css('width', $(this).outerWidth() + 'px').css('height', $(this).outerHeight() + 'px');
                });
                item.css('width', item.outerWidth() + 'px').css('height', item.outerHeight() + 'px');
            }
        },
        startDrag : function(dis) {
            var item = $(dis).closest('.password__item');
            dragMethods.itemNow = item;

            dragMethods.mouseStillDown = true;
            dragMethods.setSizes();
            dragMethods.itemNow.addClass('password__item_drag');
            dragMethods.addBlankItem();

            // Add blank item
        },
        stopDrag : function() {

            if( dragMethods.mouseStillDown === true )
            {
                dragMethods.mouseStillDown = false;

                dragMethods.itemNow = null;
                $('.password__item_drag').css('width', 'auto').css('height', 'auto').removeClass('password__item_drag');
                dragMethods.removeBlankItems();

                // Save data
                dosMethods.sendData();
            }
        }
    };

    $(document.body).on('mousedown', '.password__move', function(e) {
        e.preventDefault();
        console.log('.password__move mousedown');

        dragMethods.moveAt(e);
        dragMethods.startDrag(this);
    });

    $(document.body).on('mouseup', function(e) {
        e.preventDefault();
        console.log('.password__move mouseup');

        dragMethods.stopDrag();
    });

    $(document.body).on('mousemove', '.password', function(e) {
        if(dragMethods.mouseStillDown === true) {
            dragMethods.moveAt(e);
        }
    });
});
