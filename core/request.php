<?php
/**
 * Created by PhpStorm.
 * User: CaguCT
 * Date: 1/14/18
 * Time: 15:47
 */

namespace Core;

class Request {

	/**
	 * @param  string $url
	 * @param string  $code
	 */
	static function redirect($url, $code = '301')
	{
		header("HTTP/1.1 {$code} Moved Permanently");
		header("Location: {$url}");
		exit();
	}
}