jQuery(function($) {
    console.log('App is running');

    if($('.login__exp-counter').length > 0) {
        dosMethods.expCounter();
    }

    $(document.body).on('click', '.password__button-save', function(e) {
        e.preventDefault();

        // Send data
        dosMethods.sendData();
    });
    $(document.body).on('click', '.password__remove', function(e) {
        e.preventDefault();
        console.log('password__remove click');

        var form = $(this).closest('form.password');

        // Remove string
        $(this).closest('tr').remove();

        // Send form
        dosMethods.sendData(form);

        if($('.password__item').length === 0) {
            $('.password__empty-msg').css('display', 'table-row');
        }
    });
    $(document.body).on('click', '.password__button-add', function(e) {
        e.preventDefault();
        console.log('password__button click');

        var target = $(this).data('target');
        var form = $(this).closest('form.password');
        var url = '/index.php?do=getTemplatePart&template=parts/_password_tr.tpl';
        var data = {
            row : {
                title : '',
                link : '',
                login : '',
                password : '',
                description : ''
            }
        };

        dosMethods.ajax(url, data, function(result) {
            if($('.password__empty-msg').length > 0) {
                $('.password__empty-msg').css('display', 'none');
            }
            $(target).append($(result));

            // Send form
            dosMethods.sendData(form);
        });
    });

    // setInterval(function() {
    //     dosMethods.sendData(form);
    // }, 60000);

    $(document.body).on('click', '.password__input', function(e) {
        e.preventDefault();

        var text = $(this).val();

        if(text !== '') {
            if(dosMethods.copy(text) === true) {
                $(this).focus();
            }
        }
    });

    $(document.body).on('focusout', '.password__input', function(e) {
        console.log('.password input[type="text"] focusout');
        var form = $(this).closest('form.password');
        var val = $(this).val();

        if(val !== '') {
            // Send data
            dosMethods.sendData(form);
        }
    });

});