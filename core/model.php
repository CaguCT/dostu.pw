<?php
/**
 * Created by PhpStorm.
 * User: CaguCT
 * Date: 1/11/18
 * Time: 11:34
 */

namespace Core;

class Model {

	static $inst;
	public $dbFile = ROOT . DS . 'cache' . DS . 'database.db';
	private $db;

	/**
	 * @return Model
	 */
	static function getInstance()
	{
		if( self::$inst === null )
		{
			self::$inst = new self();
		}

		return self::$inst;
	}

	public function __construct()
	{
		$this->createDB();
	}

	/**
	 * @return mixed
	 */
	public function getDb()
	{
		return $this->db;
	}

	/**
	 * @param string $table
	 *
	 * @return bool|mixed
	 */
	public function select($table)
	{
		$this->db = $this->getFromDb();

		if( !empty($this->db[$table]) )
		{
			return $this->db[$table];
		}

		return false;
	}

	/**
	 * @param string       $table
	 * @param array|string $data
	 *
	 * @return mixed
	 */
	public function insert($table, $data)
	{
		$this->db[$table] = $data;
		$this->saveToDb();

		return $this->db;
	}

	/**
	 * @return array|mixed
	 */
	private function getFromDb()
	{
		if( is_file($this->dbFile) )
		{
			return unserialize(file_get_contents($this->dbFile));
		}

		return null;
	}

	/**
	 *
	 */
	private function saveToDb()
	{
		if( file_put_contents($this->dbFile, serialize($this->db)) !== false )
		{
			return true;
		}

		return false;
	}

	private function createDB()
	{
		if( is_file($this->dbFile) === false )
		{
			file_put_contents($this->dbFile, '');
			chmod($this->dbFile, 0666);
		}
	}
}