var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task('sass', function() {
  return gulp.src('./view/scss/**/*.scss')
  .pipe(sass({outputStyle : 'compressed'}).on('error', sass.logError))
  .pipe(gulp.dest('./public/assets/css'));
});

gulp.task('compress', function(cb) {
  pump([
      gulp.src('view/js/*.js'),
      uglify(),
      gulp.dest('public/assets/js')
    ],
    cb
  );
});

gulp.task('watch', function() {
  gulp.watch('./view/scss/**/*.scss', ['sass']);
  gulp.watch('./view/js/**/*.js', ['compress']);
});

gulp.task('build', ['sass', 'compress']);

gulp.task('default', ['build']);
