<?php
/**
 * Created by PhpStorm.
 * User: CaguCT
 * Date: 1/11/18
 * Time: 10:04
 */

spl_autoload_register(function($class_name) {

	$class_name_array = explode('\\', $class_name);

	if( $class_name_array[0] === 'Core' )
	{
		$file_ext = 'php';
		$file     = strtolower(ROOT . DS . strtr($class_name, ['\\' => DS]) . '.' . $file_ext);

		if( is_file($file) )
		{
			include_once $file;
		}
		else
		{
			throw new Exception('Can\'t include ' . $class_name);
		}
	}
});

/**
 * @param Exception $exception
 */
function exception_handler($exception)
{
	ob_start();

	echo PHP_EOL;
	echo '<b>Fatal error</b> Uncaught Exception: ' . $exception->getMessage();
	echo ' in ' . $exception->getFile() . ':' . $exception->getLine();
	echo PHP_EOL;
	echo PHP_EOL;

	echo 'Stack trace:';
	echo PHP_EOL;
	$traces = $exception->getTrace();
	foreach( $traces as $id => $trace )
	{
		echo '#' . $id . ' ';
		if( !empty($trace['file']) )
		{
			echo $trace['file'];
			echo '(' . $trace['line'] . '): ';
		}
		else
		{
			echo '[internal function]: ';
		}

		echo $trace['function'];
		echo '(' . implode(', ', $trace['args']) . ')';

		echo PHP_EOL;
	}
	$result = ob_get_clean();

	echo '<pre>' . nl2br($result) . '</pre>';
}

set_exception_handler('exception_handler');