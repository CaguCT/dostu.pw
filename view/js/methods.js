var dosMethods = {
    ajax : function(url, data, callback) {
        $.ajax({
            method : 'POST',
            url : url,
            data : data
        }).done(function(result) {
            callback(result);
        }).fail(function(jqHLR, textStatus) {
            dosMethods.message("Request failed: " + textStatus);
            dosMethods.spinner_stop();
        });
    },
    sendData : function(form) {
        var url = '/index.php?do=saveData';
        var data = $('form.password').serialize();

        dosMethods.spinner_start();
        dosMethods.ajax(url, data, function() {
            dosMethods.spinner_stop();
            dosMethods.expCounterUpdate();
        });
    },
    message : function(msg, msg_class) {
        alert(msg);
    },
    spinner_start : function() {
        $('.password__spinner').addClass('password__spinner_start');

    },
    spinner_stop : function() {
        setTimeout(function() {
            $('.password__spinner').removeClass('password__spinner_start');
        }, 1000);
    },
    copy : function(text) {
        var textarea_class = 'copyTheText';
        var textarea = null;

        if($('.' + textarea_class).length == 0) {
            textarea = $('<textarea></textarea>');
            textarea.addClass(textarea_class)
            .css('position', 'absolute')
            .css('top', 0)
            .css('left', '-9999px');
            $(document.body).append(textarea);
        }

        if(textarea === null) {
            textarea = $('.' + textarea_class);
        }

        textarea.val(text);
        textarea.select();
        return document.execCommand("Copy");
    },
    expCounterIntervalId : null,
    expCounter : function() {
        var counter = $('.login__exp-counter');
        var counter_int = +counter.text();

        if(counter_int > 0) {
            dosMethods.expCounterIntervalId = setInterval(function() {
                counter_int--;
                if(counter_int <= 0) {
                    clearInterval(dosMethods.expCounterIntervalId);
                    dosMethods.removeAll();
                    counter.text(0);
                } else {
                    counter.text(counter_int);
                }
            }, 1000);
        }
    },
    expCounterUpdate : function() {
        var counter = $('.login__exp-counter');
        var counter_int_def = +counter.data('exp');

        console.log('expCounterUpdate');

        // Stop expCounter
        clearInterval(dosMethods.expCounterIntervalId);
        dosMethods.expCounterIntervalId = null;

        if(counter_int_def > 0)
            counter.text(counter_int_def);

        // Start again
        dosMethods.expCounter();
    },
    removeAll : function() {
        $(document.body).remove();
    }
};