<?php
/**
 * Created by PhpStorm.
 * User: CaguCT
 * Date: 1/11/18
 * Time: 10:20
 */

namespace Core;

class View {

	static $inst;
	public $smarty;
	public $assign = [];
	const SMARTY_DIR = ROOT . DS . 'smarty' . DS . 'libs';
	const SMARTY_CACHE = ROOT . DS . 'cache' . DS . 'smarty';
	const TEMPLATE_DIR = ROOT . DS . TEMPLATE_DIRNAME;

	/**
	 * @return View
	 */
	static function getInstance()
	{
		if( self::$inst === null )
		{
			self::$inst = new self();
		}

		return self::$inst;
	}

	/**
	 * View constructor.
	 */
	public function __construct()
	{
		include_once self::SMARTY_DIR . DS . 'Smarty.class.php';
		$this->smarty = new \Smarty();
		$this->SmartyConfigure();

		$login = new Login();
		$this->assign['isLogin'] = $login->isLogin();

		$this->smarty->assign($this->assign);
	}

	/**
	 * Smarty configure
	 */
	private function SmartyConfigure()
	{
		$this->smarty->setTemplateDir(self::TEMPLATE_DIR);
		$this->smarty->setCompileDir(self::SMARTY_CACHE . DS . 'templates_c');
		$this->smarty->setConfigDir(self::SMARTY_CACHE . DS . 'configs');
		$this->smarty->setCacheDir(self::SMARTY_CACHE . DS . 'cache');
	}
}